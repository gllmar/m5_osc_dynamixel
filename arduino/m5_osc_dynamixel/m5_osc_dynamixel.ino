/*******************************************************************************

*******************************************************************************/
//  OSC MESSAGE
#define OSC_IN_PORT 10001
// \OSC MESSAGE

// uuid
#define UUID 0 // <- CHANGE UUID HERE (1 to 12, 0 for dev) 
byte UUID_MAC[] = {0xF1, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C};
byte UUID_MYIP[]  ={ 111, 111,  121,  131,  141,  151,  161,  171,  181,  191,  201,  211,  221};
byte UUID_DESTIP[]={ 37, 110,  120,  130,  140,  150,  160,  170,  180,  190,  200,  210,  220};
byte ROUTER_IP[]={192, 168, 1, 1};
//\uuid

//state
bool motor_pause=0;
float motor_speed=0.5;
float anticlockwise=1;

// ethPOE
#include <SPI.h>
#include <Ethernet2.h>


#define SCK 22
#define MISO 23
#define MOSI 33
#define CS 19

//Changer les derniers "chiffres de la mac addresse"
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFC, UUID_MAC[UUID] };
IPAddress MYIP(ROUTER_IP[0],ROUTER_IP[1],ROUTER_IP[2],UUID_MYIP[UUID]);
IPAddress DEST_IP(ROUTER_IP[0],ROUTER_IP[1],ROUTER_IP[2],UUID_DESTIP[UUID]);

// \ethPOE



// osc
#include <OSCMessage.h>
#include <EthernetUdp2.h>
#include <OSCBundle.h> //CNMAT
EthernetUDP udp;
//const unsigned int osc_out_port = OSC_OUT_PORT;
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  // buffer to hold incoming packet,

// \osc
// m5 LED
#include "M5Atom.h"

// couleur du led
#define WHITE 0xff, 0xff, 0xff
#define RED 0xff, 0x00, 0x00
#define YELLOW 0xff, 0xff, 0x00
#define GREEN 0x00, 0xff, 0x00
#define CYAN 0x00, 0xff, 0xff
#define BLUE 0x00, 0x00, 0xff
#define PINK 0xff, 0x00, 0xff
#define OFF 0x00, 0x00, 0x00
// \m5led
uint8_t DisBuff[2 + 5 * 5 * 3];
void set_m5_led(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
    DisBuff[0] = 0x05;
    DisBuff[1] = 0x05;
    for (int i = 0; i < 25; i++)
    {
        DisBuff[2 + i * 3 + 1] = Rdata; // -> fix : grb? weird?
        DisBuff[2 + i * 3 + 0] = Gdata; //->  fix : grb? weird? 
        DisBuff[2 + i * 3 + 2] = Bdata;
    }
      M5.dis.displaybuff(DisBuff);
}
// \couleur LED


// DYNAMIXEL

#include <Dynamixel2Arduino.h>

// Debug messages will appear on USB/serial monitor connection.
#define DEBUG_SERIAL Serial

// This is the other tab in the Arduino IDE
#include "ESP32SerialPortHandler.cpp"

// Port and pins specific to your ESP32 configuration.
#define DXL_SERIAL   Serial2
const uint8_t DXL_DIR_PIN = SERIAL_8N1; //  DIR pin
const uint8_t DXL_RX_PIN = 32; //  RX PIN
const uint8_t DXL_TX_PIN = 26; //  TX PIN

const uint8_t DXL_ID_1 = 1;
const uint8_t DXL_ID_2 = 2;
const uint8_t DXL_ID_3 = 3;
const float DXL_PROTOCOL_VERSION = 2.0;



// New way of creating dxl
Dynamixel2Arduino dxl;

// Our custom handler with RX and TX pins specified.
ESP32SerialPortHandler esp_dxl_port(DXL_SERIAL, DXL_RX_PIN, DXL_TX_PIN, DXL_DIR_PIN);


//This namespace is required to use Control table item names
using namespace ControlTableItem;

void setup_motor()
{
  // Set custom port handler
  dxl.setPort(esp_dxl_port);

  DEBUG_SERIAL.begin(115200);   //set debugging port baudrate to 115200bps
  while(!DEBUG_SERIAL);         //Wait until the serial port is opened
    
  dxl.begin(57600);
  dxl.setPortProtocolVersion(DXL_PROTOCOL_VERSION);
  dxl.ping(DXL_ID_1);
  dxl.ping(DXL_ID_2);
  dxl.ping(DXL_ID_3);
  setup_motor_reset();
  setup_motor_velocity_mode();
}

void setup_motor_velocity_mode()
{
  // set motor in operation mode velocity
  dxl.torqueOff(DXL_ID_1);
  dxl.setOperatingMode(DXL_ID_1, OP_VELOCITY);
  dxl.torqueOn(DXL_ID_1);
  
  dxl.torqueOff(DXL_ID_2);
  dxl.setOperatingMode(DXL_ID_2, OP_VELOCITY);
  dxl.torqueOn(DXL_ID_2); 
  
  dxl.torqueOff(DXL_ID_3);
  dxl.setOperatingMode(DXL_ID_3, OP_VELOCITY);
  dxl.torqueOn(DXL_ID_3);
   
  dxl.ledOn(DXL_ID_1);
  delay(500);
  dxl.ledOn(DXL_ID_2);
  delay(500);
  dxl.ledOn(DXL_ID_3);
  delay(500);
  
  dxl.ledOff(DXL_ID_1);
  dxl.ledOff(DXL_ID_2);
  dxl.ledOff(DXL_ID_3);
  
  set_motor_speed(motor_speed);
}

void setup_motor_reset()
{
  set_m5_led(BLUE);
  dxl.torqueOff(DXL_ID_1);
  //dxl.setOperatingMode(DXL_ID_1, OP_EXTENDED_POSITION);
  dxl.setOperatingMode(DXL_ID_1, OP_POSITION);
  dxl.torqueOn(DXL_ID_1);
 
  dxl.torqueOff(DXL_ID_2);
  //dxl.setOperatingMode(DXL_ID_2, OP_EXTENDED_POSITION);
  dxl.setOperatingMode(DXL_ID_2, OP_POSITION);  //bug with motor 2?
  dxl.torqueOn(DXL_ID_2);
  
  dxl.torqueOff(DXL_ID_3);
  //dxl.setOperatingMode(DXL_ID_3, OP_EXTENDED_POSITION);
  dxl.setOperatingMode(DXL_ID_3, OP_POSITION);
  dxl.torqueOn(DXL_ID_3);


  // Limit the maximum velocity in Position Control Mode. Use 0 for Max speed
  dxl.writeControlTableItem(PROFILE_VELOCITY, DXL_ID_1, 100);
  dxl.writeControlTableItem(PROFILE_VELOCITY, DXL_ID_2, 100);
  dxl.writeControlTableItem(PROFILE_VELOCITY, DXL_ID_3, 100);
  
  dxl.setGoalPosition(DXL_ID_1, 0);
  dxl.setGoalPosition(DXL_ID_2, 0);
  dxl.setGoalPosition(DXL_ID_3, 0);
  delay(2000);
}

void set_motor_speed(float f_speed)
{
    dxl.setGoalVelocity(DXL_ID_1, f_speed*anticlockwise , UNIT_PERCENT);
    dxl.setGoalVelocity(DXL_ID_2, (f_speed)*-1.0*anticlockwise, UNIT_PERCENT);
    dxl.setGoalVelocity(DXL_ID_3, f_speed*anticlockwise, UNIT_PERCENT);
    set_m5_led(GREEN);
}

void setup_network()
{
  SPI.begin(SCK, MISO, MOSI, -1);
  Ethernet.init(CS);

  // init DHCP
  Ethernet.begin(mac,MYIP);
  udp.begin(OSC_IN_PORT);

  set_m5_led(PINK); // IF here; ethernet initialize
  
  // Print init state
  Serial.print("Motor CTL ID : ");
  Serial.print(UUID);
  Serial.print(" IP => ");
  Serial.print(Ethernet.localIP());
  Serial.print(" Listening on PORT : ");
  Serial.print(OSC_IN_PORT);
  Serial.print(" MAC ADDR END : ");
  Serial.print(mac[5],HEX);
  //Serial.print(" Unicasting TO => ");
  //Serial.print(DEST_IP);
  Serial.println();
}

void OSC_get()
{
  OSCBundle bundleIN;
  int size;
  if ( (size = udp.parsePacket()) > 0)
  {
    while (size--)
      bundleIN.fill(udp.read());

    if (!bundleIN.hasError()) {}
      bundleIN.route("/led", route_moteur_led);
      bundleIN.route("/moteur/led", route_moteur_led);
      bundleIN.route("/speed", route_moteur_speed);
      bundleIN.route("/moteur/speed", route_moteur_speed);
      bundleIN.route("/reset", route_moteur_reset);
      bundleIN.route("/moteur/pause", route_moteur_pause);
      bundleIN.route("/pause", route_moteur_pause);
      bundleIN.route("/moteur/anticlockwise", route_moteur_anticlockwise);
      bundleIN.route("/anticlockwise", route_moteur_anticlockwise);
  }
}

void route_moteur_anticlockwise(OSCMessage &msg, int addrOffset)
{
  bool aclkw=bool(msg.getFloat(0));
  if (aclkw==0)
  {
    anticlockwise=-1;
  }else{
    anticlockwise=1;
  }
  set_motor_speed(motor_speed);
}  

void route_moteur_pause(OSCMessage &msg, int addrOffset)
{
  bool pause=bool(msg.getFloat(0));
  set_pause(pause);
}

void route_moteur_reset(OSCMessage &msg, int addrOffset)
{
  setup_motor_reset();
  setup_motor_velocity_mode(); 
}

void route_moteur_speed(OSCMessage &msg, int addrOffset)
{
  float msg_in =msg.getFloat(0);
  msg_in=constrain(msg_in,-10, 10); //constrain motor speed in case of too fast    
  motor_speed=msg_in;
  set_motor_speed(motor_speed);
}

void route_moteur_led(OSCMessage &msg, int addrOffset)
{
  float msg_in =msg.getFloat(0);

  if(msg_in!=0)
  {
    dxl.ledOn(DXL_ID_1);
    dxl.ledOn(DXL_ID_2);
    dxl.ledOn(DXL_ID_3);
  } else {
    dxl.ledOff(DXL_ID_1);
    dxl.ledOff(DXL_ID_2);
    dxl.ledOff(DXL_ID_3);
  }
}

void set_pause(bool b_pause)
{
  motor_pause=b_pause;
  if (motor_pause==0)
  {
    dxl.torqueOn(DXL_ID_1);
    dxl.torqueOn(DXL_ID_2);
    dxl.torqueOn(DXL_ID_3);
    set_motor_speed(motor_speed);
    set_m5_led(GREEN);
  } else {
    set_motor_speed(0);
    dxl.torqueOff(DXL_ID_1);
    dxl.torqueOff(DXL_ID_2);
    dxl.torqueOff(DXL_ID_3);
    set_m5_led(YELLOW);
  }
}

void setup() 
{
  M5.begin(true, false, true);
  set_m5_led(RED);
  setup_motor();
  setup_network();
}


void loop() 
{
  M5.update();
  OSC_get();
  if (M5.Btn.pressedFor(1000))
  {
    setup_motor_reset();
    setup_motor_velocity_mode(); 
  } else if (M5.Btn.wasPressed())
  {
    motor_pause=!motor_pause;
    set_pause(motor_pause);
  }
}
